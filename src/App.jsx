import React from 'react';
import { useState } from "react";

export function Calculator() {
  const [displayValue, setDisplayValue] = useState("");
  const [expression, setExpression] = useState("");

  const handleInput = (value) => {
    setExpression(expression + value);
    setDisplayValue(displayValue + value);
  };

  const calculateResult = () => {
    try {
      const result = eval(expression);
      setDisplayValue(result);
      setExpression(result);
    } catch (err) {
      setDisplayValue("error");
    }
  };

  const clearDisplay = () => {
    setDisplayValue("");
    setExpression("");
  };

  return (
    <>
      <main className="w-96 h-[38rem] p-6 text-zinc-50 bg-zinc-800 flex flex-col rounded-3xl justify-between  border-2 border-zinc-600">
        <h1 className="text-3xl text-center">Calculator</h1>
        <input
          type="text"
          value={displayValue}
          readOnly
          className="w-full h-14 bg-zinc-700 rounded p-2 text-xl border border-zinc-600"
        />
        <div className="grid grid-cols-4 gap-2">
          <button
            onClick={() => clearDisplay()}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl text-purple-400 hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            CE
          </button>
          <button
            onClick={() => handleInput("1")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            1
          </button>
          <button
            onClick={() => handleInput("2")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            2
          </button>
          <button
            onClick={() => handleInput("+")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-purple-600 to-purple-500 text-xl hover:bg-gradient-to-b hover:from-purple-700 hover:to-purple-600 border border-zinc-600"
          >
            +
          </button>
          <button
            onClick={() => handleInput("3")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            3
          </button>
          <button
            onClick={() => handleInput("4")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            4
          </button>
          <button
            onClick={() => handleInput("5")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            5
          </button>
          <button
            onClick={() => handleInput("-")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-purple-600 to-purple-500 text-xl hover:bg-gradient-to-b hover:from-purple-700 hover:to-purple-600 border border-zinc-600"
          >
            -
          </button>
          <button
            onClick={() => handleInput("6")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            6
          </button>
          <button
            onClick={() => handleInput("7")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            7
          </button>
          <button
            onClick={() => handleInput("8")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            8
          </button>
          <button
            onClick={() => handleInput("*")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-purple-600 to-purple-500 text-xl hover:bg-gradient-to-b hover:from-purple-700 hover:to-purple-600 border border-zinc-600"
          >
            *
          </button>
          <button
            onClick={() => handleInput("9")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            9
          </button>
          <button
            onClick={() => handleInput("0")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            0
          </button>
          <button
            onClick={() => handleInput(".")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-zinc-700 to-zinc-600 text-xl hover:bg-gradient-to-b hover:from-zinc-800 hover:to-zinc-700 border border-zinc-600"
          >
            .
          </button>
          <button
            onClick={() => handleInput("/")}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-purple-600 to-purple-500 text-xl hover:bg-gradient-to-b hover:from-purple-700 hover:to-purple-600 border border-zinc-600"
          >
            /
          </button>
          <button
            onClick={() => calculateResult()}
            className="w-16 h-16 rounded-full shadow-md shadow-black/50 bg-gradient-to-b from-purple-600 to-purple-500 text-xl hover:bg-gradient-to-b hover:from-purple-700 hover:to-purple-600 col-end-5 border border-zinc-600"
          >
            =
          </button>
        </div>
      </main>
    </>
  );
}
